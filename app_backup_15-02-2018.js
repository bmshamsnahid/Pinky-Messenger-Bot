/*
 * Copyright 2016-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* jshint node: true, devel: true */
'use strict';

const
  bodyParser = require('body-parser'),
  config = require('config'),
  crypto = require('crypto'),
  express = require('express'),
  https = require('https'),
  request = require('request'),
  mongoose = require('mongoose');

var app = express();

app.set('port', process.env.PORT || 5000);
app.set('view engine', 'ejs');
app.use(bodyParser.json({ verify: verifyRequestSignature }));
app.use(express.static('public'));

var mongoDB = 'mongodb://127.0.0.1/softographbot';
mongoose.connect(mongoDB, {
  useMongoClient: true
});

mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.on('connected', function() {
    console.log('Mongo DB connection open for DB');
});

app.get('/mongo', (req, res) => {
  // res.sendFile(__dirname + '/index.html');


  db.collection('step').find().toArray(function(err, results) {
    res.render('index', {steps: results});
  });
});

var yes = 0;
var no = 0;

/*
 * Be sure to setup your config values before running this code. You can
 * set them using environment variables or modifying the config file in /config.
 *
 */

// App Secret can be retrieved from the App Dashboard
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ?
  process.env.MESSENGER_APP_SECRET :
  config.get('appSecret');

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ?
  (process.env.MESSENGER_VALIDATION_TOKEN) :
  config.get('validationToken');

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ?
  (process.env.MESSENGER_PAGE_ACCESS_TOKEN) :
  config.get('pageAccessToken');

// URL where the app is running (include protocol). Used to point to scripts and
// assets located at this address.
const SERVER_URL = (process.env.SERVER_URL) ?
  (process.env.SERVER_URL) :
  config.get('serverURL');

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && SERVER_URL)) {
  console.error("Missing config values");
  process.exit(1);
}

/*
 * Use your own validation token. Check that the token used in the Webhook
 * setup is the same token used here.
 *
 */
app.get('/webhook', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === VALIDATION_TOKEN) {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);
  }
});


/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page.
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */
app.post('/webhook', function (req, res) {
  var data = req.body;

  // Make sure this is a page subscription
  if (data.object == 'page') {
    // Iterate over each entry
    // There may be multiple if batched
    data.entry.forEach(function(pageEntry) {
      var pageID = pageEntry.id;
      var timeOfEvent = pageEntry.time;

      // Iterate over each messaging event
      pageEntry.messaging.forEach(function(messagingEvent) {
        if (messagingEvent.optin) {
          receivedAuthentication(messagingEvent);
        } else if (messagingEvent.message) {
          receivedMessage(messagingEvent);
        } else if (messagingEvent.delivery) {
          receivedDeliveryConfirmation(messagingEvent);
        } else if (messagingEvent.postback) {
          receivedPostback(messagingEvent);
        } else if (messagingEvent.read) {
          receivedMessageRead(messagingEvent);
        } else if (messagingEvent.account_linking) {
          receivedAccountLink(messagingEvent);
        } else {
          console.log("Webhook received unknown messagingEvent: ", messagingEvent);
        }
      });
    });

    // Assume all went well.
    //
    // You must send back a 200, within 20 seconds, to let us know you've
    // successfully received the callback. Otherwise, the request will time out.
    res.sendStatus(200);
  }
});

/*
 * This path is used for account linking. The account linking call-to-action
 * (sendAccountLinking) is pointed to this URL.
 *
 */
app.get('/authorize', function(req, res) {
  var accountLinkingToken = req.query.account_linking_token;
  var redirectURI = req.query.redirect_uri;

  // Authorization Code should be generated per user by the developer. This will
  // be passed to the Account Linking callback.
  var authCode = "1234567890";

  // Redirect users to this URI on successful login
  var redirectURISuccess = redirectURI + "&authorization_code=" + authCode;

  res.render('authorize', {
    accountLinkingToken: accountLinkingToken,
    redirectURI: redirectURI,
    redirectURISuccess: redirectURISuccess
  });
});

/*
 * Verify that the callback came from Facebook. Using the App Secret from
 * the App Dashboard, we can verify the signature that is sent with each
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
  var signature = req.headers["x-hub-signature"];

  if (!signature) {
    // For testing, let's log an error. In production, you should throw an
    // error.
    console.error("Couldn't validate the signature.");
  } else {
    var elements = signature.split('=');
    var method = elements[0];
    var signatureHash = elements[1];

    var expectedHash = crypto.createHmac('sha1', APP_SECRET)
                        .update(buf)
                        .digest('hex');

    if (signatureHash != expectedHash) {
      throw new Error("Couldn't validate the request signature.");
    }
  }
}

/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to
 * Messenger" plugin, it is the 'data-ref' field. Read more at
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/authentication
 *
 */
function receivedAuthentication(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfAuth = event.timestamp;

  // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
  // The developer can set this to an arbitrary value to associate the
  // authentication callback with the 'Send to Messenger' click event. This is
  // a way to do account linking when the user clicks the 'Send to Messenger'
  // plugin.
  var passThroughParam = event.optin.ref;

  console.log("Received authentication for user %d and page %d with pass " +
    "through param '%s' at %d", senderID, recipientID, passThroughParam,
    timeOfAuth);

  // When an authentication is received, we'll send a message back to the sender
  // to let them know it was successful.
  sendTextMessage(senderID, "Authentication successful");
}

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message'
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
 *
 * For this example, we're going to echo any text that we get. If we get some
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've
 * created. If we receive a message with an attachment (image, video, audio),
 * then we'll simply confirm that we've received the attachment.
 *
 */
function receivedMessage(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;

  console.log("Received message for user %d and page %d at %d with message:",
    senderID, recipientID, timeOfMessage);
  console.log(JSON.stringify(message));

  var isEcho = message.is_echo;
  var messageId = message.mid;
  var appId = message.app_id;
  var metadata = message.metadata;

  // You may get a text or attachment but not both
  var messageText = message.text;
  var messageAttachments = message.attachments;
  var quickReply = message.quick_reply;

  if (isEcho) {
    // Just logging message echoes to console
    console.log("Received echo for message %s and app %d with metadata %s",
      messageId, appId, metadata);
    return;
  } else if (quickReply) {
    var quickReplyPayload = quickReply.payload;
    console.log("Quick reply for message %s with payload %s",
      messageId, quickReplyPayload);

    switch (quickReplyPayload)
    {
      case 'yes1':
        sendQuickReply2(senderID);
        break;

      case 'no1':
        sendQuickReply3(senderID);
        break;
      case 'yes2':
        sendQuickReply4(senderID);
        break;

      case 'no2':
        sendQuickReply5(senderID);
        break;

      case 'yes3':
      case 'no3':
        sendQuickReply6(senderID);
        break;

      case 'yes4':
        yes++;
        sendQuickReply7(senderID);
        break;
      case 'no4':
        no++;
        sendQuickReply7(senderID);
        break;

      case 'yes5':
        yes++;
        sendQuickReply8(senderID);
        break;
      case 'no5':
        no++;
        sendQuickReply8(senderID);
        break;

      case 'yes6':
        yes++;
        sendQuickReply9(senderID);
        break;
      case 'no6':
        no++;
        sendQuickReply9(senderID);
        break;

      case 'yes7':
        yes++;
        sendQuickReply10(senderID);
        break;
      case 'no7':
        no++;
        sendQuickReply10(senderID);
        break;

      case 'yes8':
        yes++;
        sendQuickReply11(senderID);
        break;
      case 'no8':
        no++;
        sendQuickReply11(senderID);
        break;

      case 'yes9':
        yes++;
        sendQuickReply12(senderID);
        break;
      case 'no9':
        no++;
        sendQuickReply12(senderID);
        break;

      case 'yes10':
        yes++;
        sendQuickReply13(senderID);
        break;
      case 'no10':
        no++;
        sendQuickReply13(senderID);
        break;

      case 'yes11':
        yes++;
        sendQuickReply14(senderID);
        break;
      case 'no11':
        no++;
        sendQuickReply14(senderID);
        break;

      case 'yes12':
        yes++;
        if (no == 9) sendQuickReply15(senderID, yes, no);
        else if (yes == 1 || yes == 2 || yes == 3) sendQuickReply16(senderID, yes, no);
        else if (yes >= 4) sendQuickReply17(senderID, yes, no);
        yes = 0;
        no = 0;
        break;
      case 'no12':
        no++;
        if (no == 9) sendQuickReply15(senderID, yes, no);
        else if (yes == 1 || yes == 2 || yes == 3) sendQuickReply16(senderID, yes, no);
        else if (yes >= 4) sendQuickReply17(senderID, yes, no);
        yes = 0;
        no = 0;
        break;

      case 'positive':
        sendQuickReply18(senderID);
        break;

      case 'yes13':
        sendQuickReply19(senderID);
        break;

      case 'no13':
        sendQuickReply20(senderID);
        break;

      case 'yes14':
      case 'yes15':
        sendQuickReply21(senderID);
        break;

      case 'no14':
        sendQuickReply22(senderID);
        break;

      case 'yes16':
      case 'yes17':
        sendQuickReply23(senderID);
        break;

      case 'yes18':
        sendQuickReply24(senderID);
        break;

      case 'yes19':
        sendQuickReply25(senderID);
        break;

      case 'yes20':
        sendQuickReply26(senderID);
        break;

      case 'no15':
      case 'no16':
      case 'no17':
      case 'no18':
      case 'no19':
      case 'no20':
        sendTextMessage(senderID, "ধন্যবাদ");
        break;

      case 'none':
        sendQuickReply15(senderID);
        break;

      case 'end':
      default:
        sendTextMessage(senderID, "আপনার সহযোগিতার জন্য ধন্যবাদ");
    }

    // sendTextMessage(senderID, "Quick reply tapped");
    return;
  }

  if (messageText) {

    // If we receive a text message, check to see if it matches any special
    // keywords and send back the corresponding example. Otherwise, just echo
    // the text we received.
    switch (messageText.replace(/[^\w\s]/gi, '').trim().toLowerCase()) {
      case 'hello':
      case 'hi':
        sendHiMessage(senderID);
        break;

      case 'image':
        requiresServerURL(sendImageMessage, [senderID]);
        break;

      case 'gif':
        requiresServerURL(sendGifMessage, [senderID]);
        break;

      case 'audio':
        requiresServerURL(sendAudioMessage, [senderID]);
        break;

      case 'video':
        requiresServerURL(sendVideoMessage, [senderID]);
        break;

      case 'file':
        requiresServerURL(sendFileMessage, [senderID]);
        break;

      case 'button':
        sendButtonMessage(senderID);
        break;

      case 'generic':
        requiresServerURL(sendGenericMessage, [senderID]);
        break;

      case 'receipt':
        requiresServerURL(sendReceiptMessage, [senderID]);
        break;

      case 'qr':
        sendQuickReply(senderID);
        break;

      case 'read receipt':
        sendReadReceipt(senderID);
        break;

      case 'typing on':
        sendTypingOn(senderID);
        break;

      case 'typing off':
        sendTypingOff(senderID);
        break;

      case 'account linking':
        requiresServerURL(sendAccountLinking, [senderID]);
        break;

      default:
        sendTextMessage(senderID, messageText);
    }
  } else if (messageAttachments) {
    sendTextMessage(senderID, "Message with attachment received");
  }
}

/*
 * Delivery Confirmation Event
 *
 * This event is sent to confirm the delivery of a message. Read more about
 * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-delivered
 *
 */
function receivedDeliveryConfirmation(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var delivery = event.delivery;
  var messageIDs = delivery.mids;
  var watermark = delivery.watermark;
  var sequenceNumber = delivery.seq;

  if (messageIDs) {
    messageIDs.forEach(function(messageID) {
      console.log("Received delivery confirmation for message ID: %s",
        messageID);
    });
  }

  console.log("All message before %d were delivered.", watermark);
}


/*
 * Postback Event
 *
 * This event is called when a postback is tapped on a Structured Message.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/postback-received
 *
 */
function receivedPostback(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfPostback = event.timestamp;

  // The 'payload' param is a developer-defined field which is set in a postback
  // button for Structured Messages.
  var payload = event.postback.payload;

  console.log("Received postback for user %d and page %d with payload '%s' " +
    "at %d", senderID, recipientID, payload, timeOfPostback);

  // When a postback is called, we'll send a message back to the sender to
  // let them know it was successful
  sendTextMessage(senderID, "Postback called");
}

/*
 * Message Read Event
 *
 * This event is called when a previously-sent message has been read.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-read
 *
 */
function receivedMessageRead(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;

  // All messages before watermark (a timestamp) or sequence have been seen.
  var watermark = event.read.watermark;
  var sequenceNumber = event.read.seq;

  console.log("Received message read event for watermark %d and sequence " +
    "number %d", watermark, sequenceNumber);
}

/*
 * Account Link Event
 *
 * This event is called when the Link Account or UnLink Account action has been
 * tapped.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
 *
 */
function receivedAccountLink(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;

  var status = event.account_linking.status;
  var authCode = event.account_linking.authorization_code;

  console.log("Received account link event with for user %d with status %s " +
    "and auth code %s ", senderID, status, authCode);
}

/*
 * If users came here through testdrive, they need to configure the server URL
 * in default.json before they can access local resources likes images/videos.
 */
function requiresServerURL(next, [recipientId, ...args]) {
  if (SERVER_URL === "to_be_set_manually") {
    var messageData = {
      recipient: {
        id: recipientId
      },
      message: {
        text: `
We have static resources like images and videos available to test, but you need to update the code you downloaded earlier to tell us your current server url.
1. Stop your node server by typing ctrl-c
2. Paste the result you got from running "lt —port 5000" into your config/default.json file as the "serverURL".
3. Re-run "node app.js"
Once you've finished these steps, try typing “video” or “image”.
        `
      }
    }

    callSendAPI(messageData);
  } else {
    next.apply(this, [recipientId, ...args]);
  }
}

function sendHiMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `
Congrats on setting up your Messenger Bot!

Right now, your bot can only respond to a few words. Try out "quick reply", "typing on", "button", or "image" to see how they work. You'll find a complete list of these commands in the "app.js" file. Anything else you type will just be mirrored until you create additional commands.

For more details on how to create commands, go to https://developers.facebook.com/docs/messenger-platform/reference/send-api.
      `
    }
  }

  callSendAPI(messageData);
}

/*
 * Send an image using the Send API.
 *
 */
function sendImageMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "image",
        payload: {
          url: SERVER_URL + "/assets/rift.png"
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a Gif using the Send API.
 *
 */
function sendGifMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "image",
        payload: {
          url: SERVER_URL + "/assets/instagram_logo.gif"
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send audio using the Send API.
 *
 */
function sendAudioMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "audio",
        payload: {
          url: SERVER_URL + "/assets/sample.mp3"
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a video using the Send API.
 *
 */
function sendVideoMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "video",
        payload: {
          url: SERVER_URL + "/assets/allofus480.mov"
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a file using the Send API.
 *
 */
function sendFileMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "file",
        payload: {
          url: SERVER_URL + "/assets/test.txt"
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a text message using the Send API.
 *
 */
function sendTextMessage(recipientId, messageText) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: messageText,
      metadata: "DEVELOPER_DEFINED_METADATA"
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a button message using the Send API.
 *
 */
function sendButtonMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "button",
          text: "This is test text",
          buttons:[{
            type: "web_url",
            url: "https://www.oculus.com/en-us/rift/",
            title: "Open Web URL"
          }, {
            type: "postback",
            title: "Trigger Postback",
            payload: "DEVELOPER_DEFINED_PAYLOAD"
          }, {
            type: "phone_number",
            title: "Call Phone Number",
            payload: "+16505551234"
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a Structured Message (Generic Message type) using the Send API.
 *
 */
function sendGenericMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "rift",
            subtitle: "Next-generation virtual reality",
            item_url: "https://www.oculus.com/en-us/rift/",
            image_url: SERVER_URL + "/assets/rift.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/rift/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for first bubble",
            }],
          }, {
            title: "touch",
            subtitle: "Your Hands, Now in VR",
            item_url: "https://www.oculus.com/en-us/touch/",
            image_url: SERVER_URL + "/assets/touch.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/touch/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for second bubble",
            }]
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a receipt message using the Send API.
 *
 */
function sendReceiptMessage(recipientId) {
  // Generate a random receipt ID as the API requires a unique ID
  var receiptId = "order" + Math.floor(Math.random()*1000);

  var messageData = {
    recipient: {
      id: recipientId
    },
    message:{
      attachment: {
        type: "template",
        payload: {
          template_type: "receipt",
          recipient_name: "Peter Chang",
          order_number: receiptId,
          currency: "USD",
          payment_method: "Visa 1234",
          timestamp: "1428444852",
          elements: [{
            title: "Oculus Rift",
            subtitle: "Includes: headset, sensor, remote",
            quantity: 1,
            price: 599.00,
            currency: "USD",
            image_url: SERVER_URL + "/assets/riftsq.png"
          }, {
            title: "Samsung Gear VR",
            subtitle: "Frost White",
            quantity: 1,
            price: 99.99,
            currency: "USD",
            image_url: SERVER_URL + "/assets/gearvrsq.png"
          }],
          address: {
            street_1: "1 Hacker Way",
            street_2: "",
            city: "Menlo Park",
            postal_code: "94025",
            state: "CA",
            country: "US"
          },
          summary: {
            subtotal: 698.99,
            shipping_cost: 20.00,
            total_tax: 57.67,
            total_cost: 626.66
          },
          adjustments: [{
            name: "New Customer Discount",
            amount: -50
          }, {
            name: "$100 Off Coupon",
            amount: -100
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a message with Quick Reply buttons.
 *
 */
function sendQuickReply(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: "স্তন ক্যান্সার কি জানতে চান?",
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes1"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no1"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply2(recipientId){
    db.collection('step').count(function (err, count) {
      if (err) return reject(err);
      sendQuickReply2Inner(recipientId,count)
    })
}

function sendQuickReply2Inner(recipientId, count) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনের কিছু কোষ যখন অস্বাভাবিকভাবে বেড়ে উঠে তখন স্তন ক্যান্সার হতে দেখা যায়।
            আপনি কি জানতে চান, আপনি স্তন ক্যান্সারের জন্য ঝুঁকিতে আছেন কিনা?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা" + count,
          "payload":"yes2"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no2"
        }
      ]
    }
  };

  var activity = {
      activity_id: 1,
      step_id: 1,
      user_id: recipientId,
      answer_id: 2
  };

  // db.user.insert({first_name: "John", last_name: "Doe"});

  db.collection('activity').save(activity, (err, result) => {
    if (err) return console.log(err);

    console.log('saved to database');
    // res.redirect('/')
  });

  callSendAPI(messageData);
}

function sendQuickReply3(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `প্রত্যেকে বিশেষ করে নারীদের ৬ মাস পর পর ঘরে বসেই স্তন ক্যান্সারের ঘরে বসে পরীক্ষা গুলো করা প্রয়োজন।
            আপনি কি জানতে চান, আপনি স্তন ক্যান্সারের জন্য ঝুঁকিতে আছেন কিনা?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes2"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no2"
        }
      ]
    }
  };

  var tagline = "Any code of your own that you haven't looked at for six or more months might as well have been written by someone else.";

  var objHash = {
      first_name: "Mem",
      last_name: "ber"
};

  // db.user.insert({first_name: "John", last_name: "Doe"});

  db.collection('user').save(objHash, (err, result) => {
    if (err) return console.log(err);

    console.log('saved to database');
    // res.redirect('/')
  });

  callSendAPI(messageData);
}

function sendQuickReply4(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `যাদের স্তন ক্যান্সার হওয়ার সম্ভাবনা বেশি রয়েছে তারা হলেন :
            *পুরুষদের চেয়ে মহিলাদের স্তন ক্যান্সার হওয়ার ঝুঁকি বেশি
            *৬০ বছর বয়সের বেশি মহিলাদের
            *একটি স্তনে ক্যান্সার হলে অপরটিও আক্রান্ত হতে পারে
            *মা, বোন অথবা মেয়ের স্তন ক্যান্সার থাকলে
            *জীনগত কারণে
            *রশ্মির বিচ্ছুরণ থেকে (Radiation Exposure)
            *অস্বাভাবিক মোটা হলে
            *অল্প বয়সে মাসিক হলে
            *বেশি বয়সে মাসিক বন্ধ হলে
            *বেশি বয়সে প্রথম বাচ্চা নিলে
            *মহিলারা যারা হরমোন থেরাপী নেন
            *মদ পান করলে।
            স্তন ক্যান্সার হয়েছে কি না নিজে থেকেই প্রাথমিকভাবে জানতে চান?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes3"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no3"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply5(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `সারা পৃথিবীতেই নারীদের মধ্যে সবচেয়ে বেশি যে ক্যান্সারটি হয় তার নাম স্তন ক্যান্সার। যা শুরুতে নির্ণয় করা গেলে চিকিতসায় পুরোপুরি ভালো হয়ে যায়।
            স্তন ক্যান্সার হয়েছে কি না নিজে থেকেই প্রাথমিকভাবে জানতে চান?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes3"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no3"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply6(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `পরিবারের কারও স্তন ক্যান্সার আছে? ফুফু, খালা, মা, নানি, দাদি বা বোন`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes4"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no4"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply7(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনে একটি পিন্ডের মত অনুভব হয়?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes5"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no5"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply8(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনের বোঁটা থেকে রক্ত বের হয়?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes6"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no6"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply9(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনের আকার ও আকৃতির পরিবর্তন হয়?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes7"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no7"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply10(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনের ত্বকে পরিবর্তন দেখা দেয়? যেমন-টোল পড়া`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes8"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no8"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply11(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনের বোঁটা ভিতরের দিকে ঢুকে যায়?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes9"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no9"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply12(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনের বোঁটার চামড়া উঠতে থাকে?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes10"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no10"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply13(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তনের ত্বক লালচে যেমন-কমলার খোসার মতো এবং গর্ত-গর্ত হয়ে যায়`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes11"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no11"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply14(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `অনেক সময় মাসিকের সময় স্তনে পিন্ড হয়, কিন্তু মাসিক পার হয়ে গেলেও পিন্ড কি থেকে যায়?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes12"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no12"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply15(recipientId, yes, no) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `আপনি আশংকামুক্ত। শুভকামনা আপনার জন্য।` + yes + ', ' + no,
      quick_replies: [
        {
          "content_type":"text",
          "title":"আমি আবার পরীক্ষা শুরু করতে চাই",
          "payload":"again"
        },
        {
          "content_type":"text",
          "title":"আমি শেষ করতে চাই",
          "payload":"end"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply16(recipientId, yes, no) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `আপনার ১-৩ লক্ষণ আছে। আপনি একটি মেমোগ্রাম (Mammogram) বা স্তনের আল্ট্রাসোনগ্রাম করান, তারপর এখানে এসে এর উত্তর দিন, 'রিপোর্ট এ কি আছে?'` + yes + ', ' + no,
      quick_replies: [
        {
          "content_type":"text",
          "title":"সমস্যা নেই",
          "payload":"none"
        },
        {
          "content_type":"text",
          "title":"ফাইব্রোএডেনোমা আছে",
          "payload":"positive"
        },
        {
          "content_type":"text",
          "title":"গ্রোথ আছে",
          "payload":"positive"
        },
        {
          "content_type":"text",
          "title":"মাস আছে",
          "payload":"positive"
        },
        {
          "content_type":"text",
          "title":"অন্য কিছুে",
          "payload":"positive"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply17(recipientId, yes, no) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `আপনার ৩ এর বেশি লক্ষণ আছে। আপনি একটি মেমোগ্রাম (Mammogram) বা স্তনের আল্ট্রাসোনগ্রাম করান, তারপর এখানে এসে এর উত্তর দিন, 'রিপোর্ট এ কি আছে?'` + yes + ', ' + no,
      quick_replies: [
        {
          "content_type":"text",
          "title":"সমস্যা নেই",
          "payload":"none"
        },
        {
          "content_type":"text",
          "title":"ফাইব্রোএডেনোমা আছে",
          "payload":"positive"
        },
        {
          "content_type":"text",
          "title":"গ্রোথ আছে",
          "payload":"positive"
        },
        {
          "content_type":"text",
          "title":"মাস আছে",
          "payload":"positive"
        },
        {
          "content_type":"text",
          "title":"অন্য কিছুে",
          "payload":"positive"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply18(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `ক্যান্সার না হলেও এমন হতে পারে। আপনি একজন চিলিতসকের পরামর্শ নিন।
আপনি কোথায় চিকিৎসা করাবেন জানতে চান? বা সরাসরি একজন জিপি ডাক্তারের সংগে ফোনে পরামর্শ করতে চান?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes13"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no13"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply19(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `*জেলা সদর হাসপাতাল
*মেডিকেল কলেজ হাসপাতাল
*বঙ্গবন্ধু শেখ মুজিব মেডিকেল বিশ্ববিদ্যালয়
*বিশেষায়িত সরকারী/বেসরকারী হাসপাতাল
অথবা বিনামুল্যে ফোনে পরামর্শ করতে পারেন।............  এই নাম্বারে।
কি ধরণের পরীক্ষা-নিরীক্ষার প্রয়োজন হতে পারে সে সম্পর্কে ধারনা পেতে চান?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes14"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no14"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply20(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `কি ধরণের পরীক্ষা-নিরীক্ষার প্রয়োজন হতে পারে সে সম্পর্কে ধারনা পেতে চান?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes15"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no15"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply21(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `*মেমোগ্রাম বা স্তনের আলট্রাসাউন্ড
*ব্রেস্ট ম্যাগনেটিক রিজোন্যান্স ইমাজিং (
*বায়োপসি
* এফএনএসি
*রক্তের পরীক্ষা
*বুকের এক্স-রে
*কম্পিউটারাইজড টমোগ্রাফী স্ক্যান
*পজিট্রন ইমিশন টমোগ্রাফী স্ক্যান
( সব গুলো সবার ক্ষেত্রে লাগবেনা)
কি ধরণের চিকিৎসা আছে সে সম্পরকে জানতে আগ্রহী?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes16"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no16"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply22(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `কি ধরণের চিকিৎসা আছে সে সম্পরকে জানতে আগ্রহী?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes17"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no17"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply23(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তন ক্যান্সোরের চিকিৎসা নির্ভর করে স্তন ক্যান্সারের ধরণ, পর্যায় ক্যান্সারের কোষগুলো হরমোণ সংবেদনশীল কিনা তার উপর। অধিকাংশ মহিলারাই স্তন অপারেশনের পাশাপাশি অন্যান্য বাড়তি চিকিৎসাও গ্রহণ করে থাকেন। যেমন: কেমোথেরাপী,হরমোণ থেরাপী অথবা রশ্মি থেরাপী ।
      অপারেশন লাগলে কী ধরণের অপারেশন হতে পারে, জানতে চান?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes18"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no18"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply24(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `স্তন ক্যান্সারের চিকিৎসার জন্য সাধারণত যে অপারেশনগুলোর করার প্রয়োজন হয়:
*ল্যাম্পপেকটমি ( শুধু টিউমারটি কাটা)
*ম্যাসটেকটমি (পুরো স্তন কেটে ফেলা, এক্ষেত্রে প্লাস্টিক সার্জনের মাধ্যমে স্তন রিকন্সট্রাকশন সার্জারি করিয়ে স্তনকে পূর্বের আকৃতিও দেয়া যায়)

স্তন ক্যান্সার বিষয়ে সচেতন থাকার জন্য  আপনাকে ধন্যবাদ। অনেকে স্তন সুগঠিত রাখা নিয়ে দুশ্চিন্তা করেন। আপনি কি স্তন সুগঠিত রাখার উপায় নিয়ে জানতে আগ্রহী?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes19"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no19"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply25(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: `নিয়মিত স্তনের ব্যায়াম করতে হবে।
নিয়ম জানাতে চান?`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"হ্যা",
          "payload":"yes20"
        },
        {
          "content_type":"text",
          "title":"না",
          "payload":"no20"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

function sendQuickReply26(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: ` যদিও কোনোরকম ব্যায়াম স্তনের আয়তন বা আকৃতি পুরো পাল্টাতে পারে না, তবুও কিছু ব্যায়ামের সাহায্যে স্তনকে আরো দৃঢ় এবং পূর্ণস্ফীত করা যায়। স্তনের তলায় অবস্থিত পেক্টোরাল (পাঁজর সংলগ্ন) মাংস পেশিটিকে সবলতর করলে, এই অঙ্গটিকে অনেক বেশি স্ফীত করা যায়। ব্যায়াম আরম্ভ করার আগে দেহভঙ্গির ব্যাপারে সচেতন হোন। সামনে ঝুঁকে দাঁড়ালে স্তনের আকার শিথিল হয়ে যেতে পারে। পিঠ সোজা করে দাঁড়ান, বুক চিতিয়ে শুরু করুন ব্যায়াম।

১) আয়নার সামনে দাঁড়িয়ে হাত দুটিকে সামনে ছড়িয়ে দিন এমনভাবে যাতে হাত দুটি একে অপরের চেয়ে ১০ সেমি দূরে থাকে। এবার হাত দুটি ওঠানামা করুন – যেন সাঁতার কাটছেন। এবাবে ১০ বার করুন।

২) এবার হাত দুটি বুকের কাছে এনে একসঙ্গে মুঠো করুন এবং একে অপরের দিকে চাপ দিন। মনে মনে ৫ গুনুন। তারপর ছেড়ে দিন। এভাবে ১০ বার করবেন।

৩) হাত দুটি বুকের কাছে এনে একটি হাত দিয়ে অপরটি ধরুন। এবার দুটি হাত দু দিকে টানুন। ৫ গুনুন। এভাবে ১০ বার করুন।

৪) হাত দুটি কাঁধ বরাবর দু দিকে ছড়িয়ে দিন। হাতের তালু দুটি নিচের দিকে রাখুন। এবার হাত দুটি কাঁধ বরাবর রেখে সামনে পিছনে করুন ২০ বার। এভাবে ১০ বার করুন।

৫) একবারে সোজা হয়ে দাঁড়ান। কান বরাবর হাত দুটি উপরে তুলুন। এবার হাত দুটি দিয়ে একটি ডাম্বেল ধরুন এবং কনুই ভাঁজ করে হাত দুটি পিছনে নামান, যাতে ডাম্বেলটি আপনার পিঠে স্পর্শ করে। এভাবে ১০ বার করুন।

৬) একটি বেঞ্চের উপর শুয়ে পড়ুন। পা দুটো ঝুলিয়ে দিন। দুটি হাতে দুটি ডাম্বেল নিন। হাত দুটি আস্তে আস্তে মাথার উপর তুলে পেটের কাছে নামিয়ে আনুন। আবার ধীরে ধীরে হাত তুলে মাথার উপর দিয়ে নিয়ে পিছনে ঝুলিয়ে দিন। এভাবে ১০ বার করুন।`,
      quick_replies: [
        {
          "content_type":"text",
          "title":"আমি আবার পরীক্ষা শুরু করতে চাই",
          "payload":"again"
        },
        {
          "content_type":"text",
          "title":"আমি শেষ করতে চাই",
          "payload":"end"
        }
      ]
    }
  };

  callSendAPI(messageData);
}

/*
 * Send a read receipt to indicate the message has been read
 *
 */
function sendReadReceipt(recipientId) {
  console.log("Sending a read receipt to mark message as seen");

  var messageData = {
    recipient: {
      id: recipientId
    },
    sender_action: "mark_seen"
  };

  callSendAPI(messageData);
}

/*
 * Turn typing indicator on
 *
 */
function sendTypingOn(recipientId) {
  console.log("Turning typing indicator on");

  var messageData = {
    recipient: {
      id: recipientId
    },
    sender_action: "typing_on"
  };

  callSendAPI(messageData);
}

/*
 * Turn typing indicator off
 *
 */
function sendTypingOff(recipientId) {
  console.log("Turning typing indicator off");

  var messageData = {
    recipient: {
      id: recipientId
    },
    sender_action: "typing_off"
  };

  callSendAPI(messageData);
}

/*
 * Send a message with the account linking call-to-action
 *
 */
function sendAccountLinking(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "button",
          text: "Welcome. Link your account.",
          buttons:[{
            type: "account_link",
            url: SERVER_URL + "/authorize"
          }]
        }
      }
    }
  };

  callSendAPI(messageData);
}

/*
 * Call the Send API. The message data goes in the body. If successful, we'll
 * get the message id in a response
 *
 */
function callSendAPI(messageData) {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: PAGE_ACCESS_TOKEN },
    method: 'POST',
    json: messageData

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      if (messageId) {
        console.log("Successfully sent message with id %s to recipient %s",
          messageId, recipientId);
      } else {
        console.log("Successfully called Send API for recipient %s",
          recipientId);
      }
    } else {
      console.error("Failed calling Send API", response.statusCode, response.statusMessage, body.error);
    }
  });
}

// Start server
// Webhooks must be available via SSL with a certificate signed by a valid
// certificate authority.
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

module.exports = app;
